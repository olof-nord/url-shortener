import { MikroORM } from "@mikro-orm/core";
import { Url } from "../entities";

const DI = {};

const initMikroORM = async () => {
    DI.orm = await MikroORM.init();
    DI.em = DI.orm.em;
    DI.urlRepository = DI.orm.em.getRepository(Url.entity);
};
initMikroORM();

export default {
    DI,
}
