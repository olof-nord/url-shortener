import uuidBase62 from "uuid-base62";
import dbService from "./dbService";
import {Url} from "../entities";

const generateShortUrl = () => {
    const base64Uuid = uuidBase62.v4();

    return base64Uuid.substring(0, 7);
};

const addUrl = async (originalUrl) => {
    const shortUrl = generateShortUrl();

    const url = new Url.entity(shortUrl, originalUrl);

    try {
        await dbService.DI.urlRepository.persistAndFlush(url);
    } catch (exception) {
        console.log(`Short URL ${shortUrl} already used, generating a new one`);
        await addUrl(originalUrl);
    }

    return shortUrl;
};

const lookupOriginalUrl = async (shortUrl) => {
    return dbService.DI.urlRepository.findOne({
        short_url: shortUrl,
    });
}

export default {
    addUrl,
    lookupOriginalUrl
}
