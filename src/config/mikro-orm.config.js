const { Url } = require("../entities");

module.exports = {
    type: "mysql",
    debug: false,
    entities: [
        Url
    ],
    migrations: {
        path: "src/migrations",
        pattern: /^[\w-]+\d+\.js$/,
        emit: "js",
    },
};
