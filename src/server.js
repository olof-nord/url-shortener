import fs from "fs";
import spdy from "spdy";
import { app } from "./app.js";

const PORT = process.env.PORT || 3000;

const spdyOptions = {
    // Private key
    key: fs.readFileSync("src/config/server.key"),

    // Cert file
    cert: fs.readFileSync("src/config/server.crt")
};

spdy.createServer(spdyOptions, app).listen(PORT, () => {
    console.log(`url-shortener is up and listening on port ${PORT}`);
});
