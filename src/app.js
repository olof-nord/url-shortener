import express from "express";
import * as openApiValidator from "express-openapi-validator";
import bodyParser from "body-parser";
import compression from "compression";
import helmet from "helmet";
const morgan = require("morgan");

import urlMiddleware from "./middleware/urlMiddleware";
import errorMiddleware from "./middleware/errorMiddleware";

export const app = express();

// support parsing of application/json type post data
app.use(bodyParser.json());

// validate incoming requests according to the API spec
app.use(
    openApiValidator.middleware({
        apiSpec: "./url-shortener.openapi.yaml",
        validateRequests: true,
        validateResponses: true,
    }),
);

// logging
app.use(morgan("common"));

app.use(compression());

// security headers
app.use(helmet());

app.post("/shorten", urlMiddleware.addUrl);
app.get("/:shortUrl", urlMiddleware.getOriginalUrl);

// error handling
app.use(errorMiddleware.errorHandler);
