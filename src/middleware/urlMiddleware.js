import urlService from "../services/urlService";

const addUrl = async (req, res, next) => {
    try {
        const originalUrl = req.body.url;
        const shortUrl = await urlService.addUrl(originalUrl);

        res.status(201).json({ shortUrl: shortUrl });
    } catch (error) {
        next(error);
    }
};

const getOriginalUrl = async (req, res, next) => {
    try {
        const shortUrl = req.params.shortUrl;
        const lookup = await urlService.lookupOriginalUrl(shortUrl);

        if(lookup === null) {
            res.status(404).send();
        }

        res.setHeader("Location", lookup.originalUrl);
        res.status(301).send();
    } catch (error) {
        next(error);
    }
};

export default {
    addUrl,
    getOriginalUrl
}
