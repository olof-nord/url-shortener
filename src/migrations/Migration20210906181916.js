'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const Migration = require('@mikro-orm/migrations').Migration;

class Migration20210906181916 extends Migration {

  async up() {
    this.addSql('create table `url` (`short_url` varchar(7) not null, `original_url` varchar(255) not null, `created_at` datetime not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `url` add primary key `url_pkey`(`short_url`);');
  }

  async down(){
    this.addSql('drop table `url`;');
  }

}
exports.Migration20210906181916 = Migration20210906181916;
