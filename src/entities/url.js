const { EntitySchema } = require("@mikro-orm/core");

class Url {
    constructor(shortUrl, originalUrl) {
        this.createdAt = new Date();
        this.shortUrl = shortUrl;
        this.originalUrl = originalUrl;
    }
}

const schema = new EntitySchema({
    class: Url,
    properties: {
        shortUrl: {
            primary: true,
            type: "string",
            length: 7
        },
        originalUrl: {
            type: "string"
        },
        createdAt: {
            type: "Date"
        },
    },
});

module.exports.Url = Url;
module.exports.entity = Url;
module.exports.schema = schema;
