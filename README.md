# URL Shortener

My very own take on an URL shortener, written with express.js in JavaScript with a MySQL backend.

## Features

* Using a single URL, output a "shorter" URL - similar to sites like bit.ly.
* Using the shorter URL should redirect to the original link.

## Example

I have the URL `https://tier.engineering/`, and I want to make a short URL.

```shell
~ curl --insecure 'https://localhost:3000/shorten' \
--header 'Content-Type: application/json' \
--data '{
    "url": "https://tier.engineering/"
}'
{"shortUrl":"2dWHdxj"}%
```

Later, I use the generated URL to redirect to the original URL.

```shell
~ curl --insecure --include 'https://localhost:3000/2dWHdxj'
HTTP/2 301
...
location: https://tier.engineering/
```

## Features

- [x] Only valid input is a valid URL.
- [x] A short URL consists of the characters `[A-Za-z0–9]`.
- [x] Only one short URL can map to one original URL.
- [ ] Links expire after 365 days.

## Setup and run

To build and start the setup locally, simply use the docker-compose file.

```bash
docker-compose up --build
```

## Development

The API is documented [here](./url-shortener.openapi.yaml)

To run without docker, `node` and `npm` is required.

To install the same node.js version which is used in the Dockerfile, use `nvm install` in the root of the repo
followed by `nvm use`.

Start the DB separately with `docker-compose up -d url-db`.

Finally, install the required npm dependencies with `npm install` and start the application with `npm start`.

To connect to the local DB, use `docker-compose exec --env MYSQL_PWD=g9DndLWTRGeBEukA6b5xLDUHr url-db mysql --database=urldb --user=dbuser`
