FROM node:14-slim

MAINTAINER Olof Nord

WORKDIR /usr/src/app

# separate dependency installation from source files.
# This allows Docker to cache these steps so that subsequent builds will be faster
COPY package.json package-lock.json ./
RUN npm ci

COPY . ./

EXPOSE 3000
ENV TZ=Europe/Berlin
USER node

CMD [ "npm", "run", "start" ]
