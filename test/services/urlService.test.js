import { v4 } from "uuid-base62";
import urlService from "../../src/services/urlService";

jest.mock("uuid-base62", () => {
    return {
        // Mock the UUID generation
        v4: jest.fn(),
    };
});

jest.mock("../../src/services/dbService", () => {
    return {
        DI: {
            // Mock the DB operations
            urlRepository: {
                persistAndFlush: jest.fn(),
                findOne: jest.fn().mockReturnValue({
                    originalUrl: "https://tier.engineering/"
                })
            }
        }
    };
});

describe("Test the URL service", () => {

    test("Given a long URL input then return a shortURL", async () => {
        const originalUrl = "https://tier.engineering/"
        const shortUrl = "3yMQGUq"

        v4.mockImplementation(() => shortUrl);

        expect(await urlService.addUrl(originalUrl)).toBe(shortUrl);
    });

    test("Given a short URL input then look up the corresponding long URL", async () => {
        const originalUrl = "https://tier.engineering/"
        const shortUrl = "3yMQGUq"

        expect(await urlService.lookupOriginalUrl(shortUrl)).toMatchObject({ originalUrl: originalUrl });
    });
});
