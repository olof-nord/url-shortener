const mockGetRequest = (parameters) => ({
    params: parameters
});

const mockPostRequest = (url) => {
    const req = {};
    req.body = {
        url: url
    };

    return req;
};

const mockResponse = () => {
    const res = {};
    res.status = jest.fn().mockReturnValue(res);
    res.json = jest.fn().mockReturnValue(res);
    res.setHeader = jest.fn().mockReturnValue(res);

    return res;
};

const mockNext = () => {
    return jest.fn();
};

export default {
    mockGetRequest,
    mockPostRequest,
    mockResponse,
    mockNext
}
