import errorMiddleware from "../../src/middleware/errorMiddleware";
import expressMocks from "../utils/mocks";
import { error } from "express-openapi-validator";

describe("Test the Error middleware", () => {
    test("Given a too short shortURL, then return HTTP error 400", async () => {
        const err = new error.BadRequest({
            "message": "request.params.shortUrl should NOT be shorter than 7 characters",
            "error": {
                "name": "Bad Request",
                "status": 400,
                "path": "/3yMQG",
                "errors": [
                    {
                        "path": ".params.shortUrl",
                        "message": "should NOT be shorter than 7 characters",
                        "errorCode": "minLength.openapi.validation"
                    }
                ]
            }
        });
        const req = expressMocks.mockGetRequest();
        const res = expressMocks.mockResponse();
        const next = expressMocks.mockNext();

        await errorMiddleware.errorHandler(err, req, res, next);

        expect(res.status).toHaveBeenCalledWith(400);
    });

    test("Given a too long shortURL, then return HTTP error 400", async () => {
        const err = new error.BadRequest({
            "message": "request.params.shortUrl should NOT be longer than 7 characters",
            "error": {
                "name": "Bad Request",
                "status": 400,
                "path": "/3yMQGUqq",
                "errors": [
                    {
                        "path": ".params.shortUrl",
                        "message": "should NOT be longer than 7 characters",
                        "errorCode": "maxLength.openapi.validation"
                    }
                ]
            }
        });
        const req = expressMocks.mockGetRequest();
        const res = expressMocks.mockResponse();
        const next = expressMocks.mockNext();

        await errorMiddleware.errorHandler(err, req, res, next);

        expect(res.status).toHaveBeenCalledWith(400);
    });
});
