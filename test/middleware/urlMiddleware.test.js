import urlMiddleware from "../../src/middleware/urlMiddleware";
import urlService from "../../src/services/urlService";
import expressMocks from "../utils/mocks";

jest.mock("../../src/services/dbService", () => {
    return {
        // Mock the MikroORM init
        orm: jest.fn(),
    };
});

describe("Test the URL middleware", () => {

    let addUrlMock = null;
    let lookupOriginalUrlMock = null;

    beforeEach(() => {
        addUrlMock = jest.spyOn(urlService, "addUrl");
        lookupOriginalUrlMock = jest.spyOn(urlService, "lookupOriginalUrl");
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    test("Given a long URL input then return shortURL", async () => {
        const originalUrl = "https://tier.engineering/"
        const shortUrl = "3yMQGUq"

        addUrlMock.mockReturnValue(shortUrl);

        const req = expressMocks.mockPostRequest(originalUrl);
        const res = expressMocks.mockResponse();
        const next = expressMocks.mockNext();

        await urlMiddleware.addUrl(req, res, next);

        expect(addUrlMock).toBeCalledTimes(1);
        expect(addUrlMock).toHaveBeenCalledWith(originalUrl);

        expect(res.status).toHaveBeenCalledWith(201);
        expect(res.json).toHaveBeenCalledWith({ shortUrl });
    });

    test("Given a short URL input then return originalURL", async () => {
        const shortUrl = "3yMQGUq"
        const longUrl = "https://tier.engineering/"

        lookupOriginalUrlMock.mockReturnValue({ originalUrl: longUrl });

        const req = expressMocks.mockGetRequest({ shortUrl });
        const res = expressMocks.mockResponse();
        const next = expressMocks.mockNext();

        await urlMiddleware.getOriginalUrl(req, res, next);

        expect(lookupOriginalUrlMock).toBeCalledTimes(1);
        expect(lookupOriginalUrlMock).toHaveBeenCalledWith(shortUrl);

        expect(res.status).toHaveBeenCalledWith(301);
        expect(res.setHeader).toHaveBeenCalledWith("Location", longUrl);
    });
});
